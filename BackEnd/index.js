const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const route = require('./api');
const cors = require('cors');
const PORT = 3100;
// Handler body parser Request
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true, limit: '2mb', parameterLimit: 1000 }));
app.use(cors());
route(app);

app.use((req,res,next) => {
  return res.status(404).json({
    success:false,
    message: 'Going somewhere',
  })
})
app.use((err,req,res,next) => {
  return res.status(500).json({
    success:false,
    message: err.message,
  })
})

app.listen(PORT ,()=>{
  console.log('server is running on port 3100')
})
