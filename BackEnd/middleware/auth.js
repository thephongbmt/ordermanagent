let moment = require('moment');
module.exports = (req,res,next) => {
  const token = req.headers['authorization'];
  if(!token){
    return res.status(401).json({
      success:false,
      message:'You are not authorize'
    })
  }
  let [username,expiredTime] = token.split('|');
  if(!username || !expiredTime){
    return res.status(401).json({
      success:false,
      message:'You are not authorize'
    })
  }
  if(moment(expiredTime).isSameOrBefore(moment())){
    return res.status(401).json({
      success:false,
      message:'Your token is expires'
    })
  }
  req.username = username;
  return next()
}