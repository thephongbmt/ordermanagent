let controller = require('./controller');
let route = require('express').Router();
module.exports = () => {
 route.post('/login', controller.login);
 return route;
}