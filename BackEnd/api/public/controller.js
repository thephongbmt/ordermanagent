const moment = require('moment');
const { account } = require('../../data');

module.exports = {
  login:(req,res)=>{
    let username = req.body.username;
    let password = req.body.password;
    let user = account.find( e => e.password === password && e.username === username);
    if(!user){
      return res.status(400).json({
        success: false,
        message: 'user is not exists'
      })
    }
    return res.status(200).json({
      success: true,
      data: {
        token:`${user.username}|${moment().add(1,'hours').format('YYYY-MM-DD HH:mm')}`
      }
    })
  }

}