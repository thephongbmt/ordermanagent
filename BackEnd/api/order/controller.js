const _ = require('lodash');
const moment = require('moment');
const { orders,provinces,countries,account } = require('../../data');
let findObjectInArray = (array = [],params = {}) => {
  let tmp = array.filter(obj => {
    let isValid = true;
    for(let key in params){
      if(!obj[key]){
        continue;
      }
      if((key!=='name' && obj[key] !== params[key]) || (key==='name' && !obj[key].toUpperCase().includes(params[key].toUpperCase()))){
        isValid = false;
        break;
      }
    }
    return isValid;
  });
  return tmp || [];
}
module.exports = {
  getCurrentUser:(req,res)=>{
    let user = account.find(e => e.username === req.username);
    return res.status(200).json({
      data: user
    })
  },
  get:(req,res)=>{
    let limit = +(req.query.limit || 10);
    let page = +(req.query.page || 1);
    let offset = limit * (page - 1);
    let params = {
      name: req.query.name,
      status: req.query.status
    }
    let total = findObjectInArray(orders,_.pickBy(params,(e) => e !== undefined)).length;
    let tmpOrder = findObjectInArray(orders,_.pickBy(params,(e) => e !== undefined)).slice(offset, page*limit);
    return res.status(200).json({
     success:true,
      data: { 
        orders:tmpOrder,
        page: page,
        total:total,
        totalPage: Math.ceil(total/limit),
        limit:limit
      }
    })
  },
  getDetail:(req,res)=>{
    let id = +req.params.id;
    let order = orders.find(or => or.id === id);
    if(!order){
      return res.status(400).json({
        success:false,
        message:'Not found with id '+id,
      })
    }
    return res.status(200).json({
      success:true,
       data:{ 
        order
      }
     })
  },
  create:(req,res)=>{
    let body = req.body;
    let params = {
      id: orders.length +1,
      name: body.name,
      status:body.status,
      price: body.price,
      provinceCode:body.provinceCode,
      countryCode:body.countryCode,
    };
    if(!params.name || !params.status){
      return res.status(400).json({
        success:false,
        message:'Missing require field'
      })
    }
    orders.push(params);
    return res.status(200).json({
      success:true,
      data:{ order: params },
    })
  },
  delete:(req,res)=>{
    let id = +req.params.id;
    if(!id){
      return res.status(400).json({
        success:false,
        message:'Missing require field'
      })
    }
    let index = orders.findIndex(or => or.id === id);
    if(index < 0){
      return res.status(400).json({
        success:false,
        message:'Not found with id '+id,
      })
    }
    orders.splice(index, 1);
    setTimeout(() => {
      return res.status(200).json({
        success:true,
        data: { id }
      })
    } ,2000)
   
  },
  update:(req,res)=>{
    let id = +req.params.id;
    if(!id){
      return res.status(400).json({
        success:false,
        message:'Missing require field'
      })
    }
    let body = req.body;
    let params = {
      id:id,
      name: body.name,
      status:body.status,
      price: body.price,
      provinceCode:body.provinceCode,
      countryCode:body.countryCode,
    };
    let index = orders.findIndex(or => or.id === id);
    if(index < 0){
      return res.status(400).json({
        success:false,
        message:'Not found with id '+id,
      })
    }
    orders[index] = params;
    return res.status(200).json({
      success:true,
      data: { order :orders[index] }
    })
  },
  getProvince: (req,res)=>{
    let countryCode = req.params.countryCode;
    let pros = provinces.filter(e => e.countryCode === countryCode);
    return res.status(200).json({
      success:true,
      data: { provinces: pros }
    })
  },
  getCountry: (req,res)=>{
    return res.status(200).json({
      success:true,
      data: {  countries: countries }
    })
  }
}