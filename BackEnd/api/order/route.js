let controller = require('./controller');
let route = require('express').Router();
module.exports = () => {
 route.get('/order', controller.get);
 route.get('/order/:id', controller.getDetail);
 route.post('/order', controller.create);
 route.delete('/order/:id', controller.delete);
 route.patch('/order/:id', controller.update);
 
 route.get('/countries', controller.getCountry);
 route.get('/countries/:countryCode/provinces', controller.getProvince);
 return route;
}