let middleware = require('../middleware/auth');
let routerOrder = require('../api/order/route');
let routePublic = require('../api/public/route');

module.exports = (app) => {
  app.use('/', routePublic());
  app.use('/api/v1', middleware , routerOrder());
}