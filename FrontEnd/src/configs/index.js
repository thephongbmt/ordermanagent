import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from "redux-saga";

import history from './history';
import createReducer from '../reducers';
import createSaga from '../saga';

const initReducer = {};
const injectReducer = {};

const sagaMiddleware = createSagaMiddleware();

const store = createStore(createReducer(injectReducer), initReducer,buildMiddleware([sagaMiddleware]));
store.runSaga = sagaMiddleware.run(createSaga);

function buildMiddleware (middlewareConfigs) {
  return applyMiddleware(...middlewareConfigs);
}

export const API_URL = process.env.API_URL || 'http://localhost:3100';
export const historyConfig = history;
export const storeConfig = store;

