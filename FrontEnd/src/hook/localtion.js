import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { getCountry } from '../action/country';
import { getProvince } from '../action/province';

const useLocation = (countryCode) => {
   
  const dispatch = useDispatch();
  const countries = useSelector(state => state.countryReducer.countries);
  const provinces = useSelector(state => state.provinceReducer.provinces);

  useEffect(() => {
    if(!countries || countries.length === 0){
       dispatch(getCountry());
    }
  });
  useEffect(() => {
    if(countryCode){
      dispatch(getProvince(countryCode));
    }
  },[countryCode, dispatch]);
  return {
    provinces,countries
  }
}

export default useLocation;