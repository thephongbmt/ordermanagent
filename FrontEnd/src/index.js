import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import history from './configs/history';
import {
  BrowserRouter  as Router,
} from 'react-router-dom'
import { Provider } from "react-redux";
import { storeConfig,historyConfig } from './configs';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
    <Provider store = {storeConfig}>
      <Router history = {historyConfig}>
        <App />
      </Router>
    </Provider>
  ,document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
