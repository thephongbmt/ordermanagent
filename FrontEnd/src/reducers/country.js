import produce from 'immer';

const initStore = {
  countries: []
}

const countryReducer = (state = initStore, { type, payload }) => produce(state, draft => {
  switch(type){
    case 'GET_COUNTRY_SUCCESS': {
      console.log('reducer');
      draft.countries = payload.countries;
      break;
    }
    default:
      return state;
  }
})
export default countryReducer;