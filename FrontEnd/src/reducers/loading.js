import produce from 'immer';

const initLoading = {
  isLoading: false
}

const loadingReducer = (state = initLoading, { type, payload }) => produce(state, (draft) => {
   switch(type){
    case 'SHOW_LOADING': draft.isLoading = true; break
    case 'HIDE_LOADING': draft.isLoading = false; break
    default: return state;
   }
});

export default loadingReducer;