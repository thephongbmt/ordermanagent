/**
 * Combine all reducers in this file and export the combined reducers.
 */
import { combineReducers } from "redux";
/** import main reducer load sync */
import countryReducer from "./country";
import provinceReducer from "./province";
import orderReducer from "./order";
import loadingReducer from "./loading";

export default function configureStore(injectStore) {
    return combineReducers({
      provinceReducer,
      countryReducer,
      orderReducer,
      loadingReducer,
      ...injectStore
  });
}