import produce from 'immer';

const initReducer = {
  order: {},
  orders:[],
  total: 0,
  page: 1,
  limit: 10,
};

const orderReducer = (state = initReducer, { type, payload }) => produce(state, (draft) => {
  switch(type){
    case 'GET_ORDER_DETAIL_SUCCESS':
      draft.order = payload.order;
      break;
    case 'GET_ORDER_LIST_SUCCESS':
      draft.orders = payload.orders;
      draft.total = payload.total;
      draft.limit = payload.limit;
      draft.total = payload.total;
      break;
    default: 
    return state;
  }
})

export default orderReducer;