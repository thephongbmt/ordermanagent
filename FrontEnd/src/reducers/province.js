import produce from 'immer';

const initProvince = {
}


const ProvinceReducer = (state = initProvince,{type,payload}) => produce(state,draft =>{
  switch(type){
    case 'GET_PROVINCE_SUCCESS': 
    draft.provinces = payload.provinces;
    break;
    default: return state;
  }
})
export default ProvinceReducer;