import Qs from 'qs';
import axios from 'axios';
import { getLocalStorage, KEY } from './localStorage';
import { API_URL } from '../configs';
const { GET, POST, PUT, PATCH, DELETE } = {
  GET: 'GET',
  POST: 'POST',
  PUT: 'PUT',
  PATCH: 'PATCH',
  DELETE: 'DELETE'
}

const setHeader = (headers = {}) => {
  let token = getLocalStorage(KEY.ACCESS_TOKEN);
  if(token) {
    headers['Authorization'] = token; 
  }
  return {
    "Content-Type": "application/json",
    "accept": "application/json;application/x-www-form-urlencoded",
    "Access-Control-Allow-Origin": "*",
    ...headers
  }
}

const requestAPI = (method, url, headers, data) => {
  headers = setHeader(headers);
  let config = {
    headers,
    url: `${API_URL}${url}`,
    method,
    paramsSerializer: function(params) {
      return Qs.stringify(params, {
        arrayFormat: "brackets"
      });
    }
  }
  if(data && method === GET){
    config.params = data
  }
  if(data)  {
    config.data = data
  }
  return axios(config)
    .then(async res => {
      return res;
    })
    .catch(err => {
      return new Promise(function(resolve, reject) {
        if (err.response) {
          return reject(err.response);
        }
        return reject(err);
      });
    });
}

export default {
  get(url, dataBody, headers = {}) {
    return requestAPI(GET, url, headers, dataBody);
  },

  post(url, dataBody, headers = {}) {
    return requestAPI(POST, url, headers, dataBody);
  },

  put(url, dataBody, headers = {}) {
    return requestAPI(PUT, url, headers, dataBody);
  },
  patch(url, dataBody, headers = {}) {
    return requestAPI(PATCH, url, headers, dataBody);
  },

  delete(url, dataBody, headers = {}) {
    return requestAPI(DELETE, url, headers, dataBody);
  }
};
