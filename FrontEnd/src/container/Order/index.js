
import React from 'react';
import Button from '../../components/Button';
import OrderList from '../OrderList';
import { useHistory } from 'react-router-dom'
const Order = function () {
  const history = useHistory();
  const handleCreate = () => {
    history.push('/order/add');
  }
  return (
    <div>
      <h1>Order Manage</h1>
      <Button onClick={handleCreate} title='Create' />
      <br />
      <br />
      <OrderList />
    </div>
  )
}

export default Order;