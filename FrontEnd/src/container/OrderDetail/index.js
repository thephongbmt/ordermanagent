import React,{ useEffect, Fragment } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';
import { Formik } from 'formik'
import { getDetail, createOrder, updateOrder } from '../../action/order'
import Form from './form';


const OrderDetail = () => {
  const order = useSelector(state => state.orderReducer.order);
  const { orderId } = useParams();
  const dispatch = useDispatch();
  console.log('OrderDetail')
  useEffect(() => {
    if(orderId) {
      dispatch(getDetail(orderId))
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  },[orderId]);

  const initForm = {
    name:'',
    countryCode: '',
    provinceCode:'',
    status: '',
    price:'',
    ...order
  };
  const handleSubmit = (data) =>{
    if(orderId){
      return dispatch(updateOrder(orderId, data));
    }
    return dispatch(createOrder(data))
  }
  return (
    <div>
      {
        orderId && Object.keys(order).length === 0 ? 
        <h4>Order is not found</h4> : 
        <Fragment>
        <h2>Order</h2>
        <Formik 
          initialValues={initForm}
          render = { props => <Form {...props} />}
          // validationSchema =  {loginValidationSchema}
          onSubmit={handleSubmit}
        />
        </Fragment>
      }
    </div>
  )
}

export default OrderDetail;