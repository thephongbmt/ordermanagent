import React,{useEffect} from 'react';
import useLocation from '../../hook/localtion';

import { convertToSelectData } from '../../helper/utilities'
import {status as statusEnum } from '../../constants'



import Label from '../../components/Lable';
import TextBox from '../../components/TextBox';
import Button from '../../components/Button';
import SelectBox from '../../components/SelectBox';

const OrderForm = (props) => {
  const {
    values: { name ,status ,countryCode ,provinceCode , price },
    handleSubmit,
    handleChange,
  } = props;

  const {provinces,countries} = useLocation(countryCode);

 // Handler
  const handleChangeInput = (e) => {
    const { name, value } = e.target;
    console.log(name,value)
    if(name === 'price' && !/[0-9]/g.test(value)){
      return;
    }
    return handleChange(e);
  };

  return (
    <form onSubmit={handleSubmit}>
      <Label title='Name' />:
      <TextBox
       name='name'  
       value={name}  
       onChange={handleChangeInput} 
       /><br />

      <Label title='Status' />
      <SelectBox
      name = 'status'
      onChange = {handleChangeInput} 
      value = {status} data={statusEnum } 
      /><br />

      <Label title = 'Country' />
      <SelectBox 
      name='countryCode'
      onChange = {handleChangeInput} 
      value = {countryCode} 
      isHaveEmptyValue={true}
      data = {convertToSelectData(countries,'name','code')} /><br />

      <Label title='Province' />
      <SelectBox 
      name='provinceCode'
      onChange={handleChangeInput} 
      value={provinceCode} 
      isHaveEmptyValue={true}
      data={convertToSelectData(provinces,'name','code')} /><br />

      <Label title='Price' />
      <TextBox 
      name='price'
      onChange={handleChangeInput} 
      value={price} 
      placeholder='Price' /><br />

      <Button title='Submit' />
    </form>
  )
}

export default OrderForm