import debounce from 'lodash.debounce'
import React, { memo, useEffect, useState, Fragment } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom'
import useLocation from '../../hook/localtion';

import Table from '../../components/Table';
import TextBox from '../../components/TextBox';
import SelectBox from '../../components/SelectBox';
import Button from '../../components/Button';

import { getList, deleteOrder } from '../../action/order';
import { status as statusEnumSelect } from '../../constants';

import { convertToSelectData } from '../../helper/utilities';

const OrderList = (props) => {
  //Hook
  const [form, setForm] = useState({
    name:'',
    status:'',
    countryCode:'',
    provinceCode:'',
    page:1,
    limit:3
  });
   const history = useHistory();
   const orders = useSelector(state => state.orderReducer.orders);
   const total = useSelector(state => state.orderReducer.total);

   const { countries, provinces } = useLocation(form.countryCode);
   const dispatch = useDispatch();

   useEffect(() => {
       dispatch(getList(form));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  },[form]);

  
  // func handle
  const handleSearch =  (e) => {
      const { name, value } = e.target;
      setForm({...form, [name]: value });
  } 
  const handleChangePage = (page) => {
    return handleSearch({ target: { name: 'page', value: page}})
  }

  const handleUpdate = (id) => {
    history.push(`/order/update/${id}`);
  }

  const handleDelete = (id) => {
    dispatch(deleteOrder(id));
  }

   const configTable = [
     {
       name:'name',
       thead: <TextBox value={form.name} name='name' onChange={handleSearch} />,
     },
     {
      name:'status',
      thead: <SelectBox value={form.status} name='status' data={statusEnumSelect} onChange={handleSearch}/>
     },
     {
      name:'countryCode',
      thead: <SelectBox value={form.countryCode}  name='countryCode' data={convertToSelectData(countries,'name','code')}  onChange={handleSearch}  />
     },
     {
      name:'provinceCode',
      thead: <SelectBox  value={form.provinceCode} name='provinceCode'   data={convertToSelectData(provinces,'name','code')}   onChange={handleSearch}  />
     },
     {
      name:'price',
      thead: 'price',
     },
     {
      name:'action',
      thead: 'action',
     },
    ];

   
    const renderData = (order) => {
      return (
        <Fragment>
          <td>{order.name}</td>
          <td>{order.status}</td>
          <td>{order.countryCode}</td>
          <td>{order.provinceCode}</td>
          <td>{order.price}</td>
          <td>
            <Button title='Update' className='success' onClick={() => handleUpdate(order.id)} />
            <Button title='Delete' className='danger' onClick={() => handleDelete(order.id)} />
          </td>
        </Fragment>
      )
    }

    return (
      <Table
      configTable={configTable}
      renderData={order => renderData(order)}
      data={orders}
      rowKey={'id'} 
      page={form.page}
      perPage={form.limit}
      totalPage={total}
      onChangePagination={handleChangePage}
      />
    )

}

export default memo(OrderList);