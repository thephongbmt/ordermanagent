
import React, { memo } from 'react';
import { Formik } from 'formik'
import LoginForm from './form';
import { useHistory } from "react-router-dom";
import { setLocalStorage, KEY } from '../../helper/localStorage'
import { loginValidationSchema } from './validation';
import requestApi from '../../helper/requestApi';
import './style.css'

const initForm = {
  username:'',
  password:''
}

const Login = () => {
  const history = useHistory();
  
  const handleLogin = (data) => {
    requestApi.post('/login',data,{}).then(res=>{
      setLocalStorage(KEY.ACCESS_TOKEN,res.data.data.token);
      history.push("/order");
    }).catch(e => {
      alert(e.data.message)
    });
  }

  return (
    <div className='wrapper-login'>
      <div className='cotainer-login-form'>
        <div className='header-login-form'>
        <p className='login-title'> Login Form</p>
        </div>  
        <div className='login-form'> 
          <Formik 
              initialValues={initForm}
              render = { props => <LoginForm {...props} />}
              validationSchema =  {loginValidationSchema}
              onSubmit={handleLogin}
          />
        </div>
      </div>
    </div>
  )
}
export default memo(Login);