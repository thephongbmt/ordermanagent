
import React, { memo } from 'react';
import TextBox from '../../components/TextBox';
import Button from '../../components/Button';
import './style.css'

const LoginForm = (props) => {
  const {
    values: { username, password },
    errors,
    touched,
    handleSubmit,
    handleChange,
    setFieldTouched,
  } = props;

  const handleChangeInput = (e) => {
    const { name } = e.target;
    handleChange(e);
    setFieldTouched(name, true, false);
  }

  return (
      <form onSubmit={ handleSubmit }>
        <p className='error-form-message'>{ touched.username ? errors.username : "" }</p>
        <TextBox
        name='username'
        className='login-textbox'
        onChange={handleChangeInput}
        value={username}
        placeholder='username' />
        <p className='error-form-message'>{touched.password ? errors.password : ""}</p>
        <TextBox 
         name='password'
        className='login-textbox'
         onChange={handleChangeInput}
         type='password'
         value={password}
         placeholder='Password' />
         <br/>
        <Button className={'login-button'}  type="submit" title={'Login'} /> 
      </form>
  )
};

export default memo(LoginForm)
