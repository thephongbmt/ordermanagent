import * as Yup from "yup";

export const loginValidationSchema = Yup.object({
  password: Yup.string().min(6,"password must have at least 6 character").required(),
  username: Yup.string().required()
})