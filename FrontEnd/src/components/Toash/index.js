import React from 'react';
import { useSelector } from 'react-redux';

import './style.css'
const Toash = ({type ='default'}) => {
  const { message, title } = useSelector(state => state.toashReducer);
 return (<div className={`toash toash-${type}`}>
   <div className='toash-title'>{type.toLocaleUpperCase() || title}</div>
<div className='toash-description'>{message}</div>
 </div>)
}

export default Toash;