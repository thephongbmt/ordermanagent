import React, { memo } from 'react';
import './style.css'
const Button = memo(({ title='Submit', ...rest}) => {
  return (
    <button {...rest}  className={`button ${rest.className}`} > {title} </button>
  )
})

export default Button;