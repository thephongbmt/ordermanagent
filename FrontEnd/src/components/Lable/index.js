import React from 'react'

const Label = (props) => {
  return (
  <label style={props.style} className={`${props.className}`} >{props.title}</label>
  )
}

export default  Label;