import React, { memo } from 'react';
import './style.css'

const Footer = ({ description='This is Footer', ...props }) => {
  return (
    <div className='admin-footer'>{description}</div>
  )
}

export default memo(Footer);