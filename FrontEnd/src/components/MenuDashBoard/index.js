import React, { memo } from 'react';
import { Link } from 'react-router-dom';
import './style.css'
const Menu = ({ title = 'Menu A', to = '/' , ...props }) => {
  console.log('Menu',props)
  return (
  <div className='menu-side-bar'>
    <Link to = {to}>{title}</Link>
  </div>
  )
}
export default memo(Menu);