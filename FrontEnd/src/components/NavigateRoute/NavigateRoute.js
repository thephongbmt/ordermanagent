import React from 'react';
import { Route, Redirect } from 'react-router-dom';

let NavigateRoute = (getToken, redirectPath) => ({ component:Component,...propsRoute }) => {
  return (
      <Route {...propsRoute}>
         {
           getToken() ?  <Component /> : <Redirect to={{ pathname:redirectPath , state: { from: propsRoute.location } }} />
         }
      </Route>
    )
}
export default NavigateRoute;
