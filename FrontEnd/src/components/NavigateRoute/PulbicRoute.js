import NavigateRoute from './NavigateRoute';
import { getLocalStorage, KEY } from '../../helper/localStorage';
let PublicRoute = NavigateRoute(() => !getLocalStorage(KEY.ACCESS_TOKEN), '/')
export default PublicRoute;
