import React, { Fragment, memo } from 'react';
import Header from '../Header';
import Footer from '../../components/Footer';
import MenuDashBoardList from '../MenuDashBoardList';
import './style.css'
const AdminLayout = (props) => {
  return (
    <Fragment>
      <Header />
      <div className='container-admin-layer'>
        <div className='container-admin-left-side-bar'>
           <MenuDashBoardList {...props} />
        </div>
        <div className='container-admin-content'>
          {props.children}
        </div>
      </div>
      <Footer />
    </Fragment>
  )
}

export default memo(AdminLayout)
