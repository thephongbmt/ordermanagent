import React, { memo } from 'react';
import Pagination from 'react-js-pagination';
import Tbody from '../Table/Tbody';
import Thead from '../Table/Thead';
import './style.css';
require('../../../node_modules/bootstrap/dist/css/bootstrap.min.css');
// import '../../../node_modules/bootstrap/dist/css/bootstrap/dist/css/bootstrap.min.css';

const Table = ({ 
  configTable, 
  data, 
  rowKey, 
  renderData, 
  page = 1 , 
  totalPage = 0, 
  perPage = 10,
  pageRange =5,
  onChangePagination 
}) => {
  return (
    <div>
    <table className='table'>
      <Thead 
      configTable={configTable}
      />
      <Tbody 
      data={data}
      configTable={configTable}
      rowKey={rowKey}
      renderData={renderData}
      />
    </table>
    <Pagination
    activePage={page}
    itemsCountPerPage={perPage}
    totalItemsCount={totalPage}
    pageRangeDisplayed={pageRange}
    onChange={onChangePagination}
  />
  </div>
  )
}

export default memo(Table);