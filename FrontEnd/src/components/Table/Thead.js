import React, { memo } from 'react';

const Thead = ({ configTable, className, ...rest }) => {
  const handleRender = () => {
    return configTable.map(e => <td key={ e.name } > { e.thead } </td>)
  }
  return  (
    <thead className={`${className}`}>
        <tr>
          {handleRender()}
        </tr>
    </thead>
  )
}

export default memo(Thead);