import React, { memo } from 'react';

const Tbody = ({ data, rowKey , configTable, renderData, className, ...rest }) => {
  const renderRow = (element, index) => {
    let tds = [];
    for(let th of configTable){
      let key = th.name;
      let value = element[key];
      tds.push(<td key={index}>{value}</td>);
    }
    return tds;
  }
  const handleRender = () => {
    if(renderData){
      return data.map(e => <tr key={e[rowKey]} >{renderData(e)}</tr>)
    }
    return data.map(e => <tr key={e[rowKey]}>{renderRow(e)}</tr>)
  }
  return  (
    <tbody className={`${className}`}>
      {handleRender(data)}
    </tbody>
  )
}

export default memo(Tbody);