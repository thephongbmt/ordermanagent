import React, { memo } from 'react';
import './style.css';
const TextBox = memo(({placeholder = 'Typing something',...rest}) => {
  return (
    <input type="text" placeholder={placeholder} {...rest} className={`textbox ${rest.className}`}  />
  )
})

export default TextBox;