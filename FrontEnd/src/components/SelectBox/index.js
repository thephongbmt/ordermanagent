import React from 'react';
import './style.css'

const SelectBox = ({isHaveEmptyValue = true, titleEmptyValue = '--Please Select--', ...rest}) => {

  let handleRenderOption = () => {
    const { data } = rest;
    if(!data){
      return;
    }
    return data.map(e => <option value={e.value} key={e.value}>{e.name}</option>)
  }
  let generateEmptyValue = () => {
    if(!isHaveEmptyValue){
      return;
    }
    return (
      <option value=''>{titleEmptyValue}</option>
    ) 
  }
  return (
    <select className='selectbox' {...rest}>
      {generateEmptyValue()}
      {handleRenderOption()}
    </select>
  )
}
export default SelectBox