import MenuDashBoard from '../MenuDashBoard';
import React ,{memo} from 'react';

const hanldeRenderMenus = (menus) => {
  console.log(menus)
  return menus.map(({path, name, ...props} )=> <MenuDashBoard key={path} to={path} title={name} {...props} />)
}

const MenuDashBoardList = ({ menus= [], ...props}) => {
  return <div>
    { hanldeRenderMenus(menus) }
  </div>
}

export default memo(MenuDashBoardList);