import React from 'react';
import { useSelector } from 'react-redux';

import './style.css'
const Loading = (props) => {
  const isLoading = useSelector(state => state.loadingReducer.isLoading);
  console.log('isLoading',isLoading)
 return (<div className='loading' hidden={!isLoading} />)
}

export default Loading;