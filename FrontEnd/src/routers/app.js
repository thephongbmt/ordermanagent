
import { lazy } from 'react';
export const DashBoardLoadable = lazy(()=> import('../container/DashBoard'));
export const LoginLoadable = lazy(()=> import('../container/Login'));
export const OrderLoadable = lazy(()=> import('../container/Order'));
export const OrderDetailLoadable = lazy(()=> import('../container/OrderDetail'));

export const initRoute = [
  // Public
  { path: '/login', isAuthenRoute:false, component: LoginLoadable, exact: true },
  // Private
  { path: '/', isAuthenRoute: true , component: DashBoardLoadable, exact: true, name: 'DashBoard', isShowSideBar:true  },
  { path: '/order', isAuthenRoute:true, component: OrderLoadable, exact: true, name: 'Order', isShowSideBar:true },
  { path: '/order/add', isAuthenRoute:true, component: OrderDetailLoadable, exact: true },
  { path: '/order/update/:orderId', isAuthenRoute:true, component: OrderDetailLoadable, exact: true },
];
