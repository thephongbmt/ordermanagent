import React from 'react';
import {
  Switch,  withRouter
} from 'react-router-dom'
import PublicRoute from '../components/NavigateRoute/PulbicRoute';
import PrivateRoute from '../components/NavigateRoute/PrivateRoute';
import AdminLayout from '../components/AdminLayout';
import { initRoute } from './app';



const filterRouteType = (routes) => {
  return {
    publicRoutes: routes.filter(route => !route.isAuthenRoute),
    privateRoutes: routes.filter(route => route.isAuthenRoute),
    menuSideBars: routes.filter(route => route.isAuthenRoute && route.isShowSideBar),
  }
}

const InitRoute = () => {
  let { publicRoutes, privateRoutes, menuSideBars } = filterRouteType(initRoute);
  return (
    <React.Suspense fallback={"..."}>
       <Switch>
       {publicRoutes.map(route => (<PublicRoute key={route.path}  {...route} />))}
        <AdminLayout menus = {menuSideBars} >
          {privateRoutes.map(route => (<PrivateRoute key={route.path} {...route} />))}
        </AdminLayout>
      </Switch>
    </React.Suspense>
  )
}

export default withRouter(InitRoute);