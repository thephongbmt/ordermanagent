export const getDetail = (id) => ({
  type:'GET_ORDER_DETAIL',
  payload: { id }
})
export const getList = (query) => ({
  type:'GET_LIST_ORDER',
  payload: { query }
})

export const createOrder = (order) => ({
  type: 'CREATE_ORDER',
  payload: { order }
})

export const deleteOrder = (id) => ({
  type:'DELETE_ORDER',
  payload: { id }
})

export const updateOrder = (id, order) => ({
  type:'UPDATE_ORDER',
  payload: { id, order }
})