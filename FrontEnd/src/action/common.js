export const reduxAction = (type,data) => ({
  type,
  payload:data
})
export const showLoading = (flag, option) => ({
  type: flag ? 'SHOW_LOADING' : 'HIDE_LOADING',
  payload: option
})
