export const getProvince = (countryCode) => ({
  type:'GET_PROVINCE',
  payload: { countryCode }
})
