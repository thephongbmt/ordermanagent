
import { put, takeLatest } from "redux-saga/effects";
import requestApi from "../helper/requestApi";
import { reduxAction, } from "../action/common";

const COUNTRY = 'countries';

function *getListCountry (action) {
  try{
    let defaultParams = {
      page: 1,
      limit: 10
    }
    let { data } = yield requestApi.get(`/api/v1/${COUNTRY}`,defaultParams);
    yield put(reduxAction('GET_COUNTRY_SUCCESS',data.data));
  } catch(err) {
    yield put(reduxAction("HANDLE_ERROR", err));
  } finally {
  
  }
}
export function* watchCountry() {
  yield takeLatest("GET_COUNTRY", getListCountry);
}
