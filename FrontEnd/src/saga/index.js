import { all } from "redux-saga/effects";
import { watchCountry } from "../saga/country";
import { watchProvince } from "../saga/province";
import { watchOrder } from "../saga/order";

export default function* rootSaga() {
  yield all([
    watchCountry(),
    watchProvince(),
    watchOrder(),
  ])
}