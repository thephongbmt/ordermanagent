
import { put, takeLatest } from "redux-saga/effects";
import requestApi from "../helper/requestApi";
import { reduxAction, } from "../action/common";

const PROVINCE = 'provinces';
const COUNTRY = 'countries';

function *getListProvince ({type,payload}) {
  try{
    let defaultParams = {
      page: 1,
      limit: 10
    }
    let { data } = yield requestApi.get(`/api/v1/${COUNTRY}/${payload.countryCode}/${PROVINCE}`,defaultParams);
    let countries = data.data;
    yield put(reduxAction('GET_PROVINCE_SUCCESS',countries));
  } catch(err) {
    yield put(reduxAction("HANDLE_ERROR", err));
  } finally {
  
  }
}
export function* watchProvince() {
  yield takeLatest("GET_PROVINCE", getListProvince);
}
