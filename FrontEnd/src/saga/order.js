import pickBy from 'lodash.pickby';
import { put, takeLatest } from "redux-saga/effects";
import { toast } from 'react-toastify';
import history from "../configs/history";
import { reduxAction, showLoading } from "../action/common";
import { getList } from '../action/order';
import requestApi from "../helper/requestApi";

const ORDER = 'order';

function *getOrderDetail ({type,payload}) {
  try{
    let { data } = yield requestApi.get(`/api/v1/${ORDER}/${payload.id}`);
    let orderDetail = data.data;
    yield put(reduxAction('GET_ORDER_DETAIL_SUCCESS',orderDetail));
  } catch(err) {
    yield put(reduxAction("HANDLE_ERROR", err));
  } finally {
  
  }
}
function *getListOrder ({type, payload: {query} }) {
  try{
    yield put(showLoading(true));
    query = pickBy(query,(e) => e);
    let { data } = yield requestApi.get(`/api/v1/${ORDER}`,query);
    let orders = data.data;
    yield put(reduxAction("GET_ORDER_LIST_SUCCESS", orders))
  } catch(err) {
    yield put(reduxAction("HANDLE_ERROR", err));
  } finally {
    yield put(showLoading(false));
  }
}
function *createOrder({ type, payload }){
  try {
    showLoading(true);
    let { data } = yield requestApi.post(`/api/v1/${ORDER}`,payload.order);
    let orders = data.data;
    yield put(reduxAction("CREATE_ORDER_SUCCESS", orders));
    // yield put(history.push('/order'));
    toast("Thêm thành công");
  } catch(err){
    yield put(reduxAction("HANDLE_ERROR", err));
    toast(err.message);
  } finally {
    yield put(showLoading(false));
  }
}
function *updateOrder({ type, payload }){
  try {
    yield put(showLoading(true));
    let { data } = yield requestApi.patch(`/api/v1/${ORDER}/${payload.id}`,payload.order);
    let orders = data.data;
    yield put(reduxAction("UPDATE_ORDER_SUCCESS", orders));
    history.push('/order');
    toast('Cập nhật thành công');
  } catch(err){
    yield put(reduxAction("HANDLE_ERROR", err));
    toast(err.message);
  } finally {
    yield put(showLoading(false));
  }
}
function *deleteOrder({ type, payload }){
  try {
    console.log('start')
    yield put(showLoading(true));
    let { data } = yield requestApi.delete(`/api/v1/${ORDER}/${payload.id}`);
    let orders = data.data;
    yield put(reduxAction("DELETE_ORDER_SUCCESS", orders));
    yield put(getList({
      page:1,
      limit:4
    }));
    toast('Xóa thành công');

  } catch(err){
    yield put(reduxAction("HANDLE_ERROR", err));
    toast(err.message);
  } finally {
    yield put(showLoading(false));
  }
}
export function* watchOrder() {
  yield takeLatest("GET_ORDER_DETAIL", getOrderDetail);
  yield takeLatest("GET_LIST_ORDER", getListOrder);
  yield takeLatest("UPDATE_ORDER", updateOrder);
  yield takeLatest("CREATE_ORDER", createOrder);
  yield takeLatest("DELETE_ORDER", deleteOrder);
}
