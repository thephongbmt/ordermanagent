import React, { Fragment } from "react";
import InitRoute from './routers';
import Loading from './components/Loading';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
export default function App(){
  return  (
    <Fragment>
      <Loading />
      <ToastContainer />
      <InitRoute />
    </Fragment>
  )
}